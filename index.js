const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4004;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MongoDB Connection
mongoose.connect("mongodb+srv://lng-athens:BVgvPPJIEu6qjm8V@zuitt-bootcamp.oquvder.mongodb.net/s35-activity?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));
db.on("open", () => console.log(
`Connected to MongoDB Atlas
Connected to Zuitt-Bootcamp database
Connected to s35-activity collection`
));

const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username}, (err, rslt) => {
        if (rslt != null && rslt.username == req.body.username) {
            return res.send("Username already taken! Please use a different one.");
        }
        
        let newUser = new User({
            username: req.body.username,
            password: req.body.password
        });
        newUser.save((err, saveUser) => {
            if (err) {
                return console.error(err);
            }
            else {
                return res.status(200).send(`User with username ${req.body.username} has been created`);
            }
        });
    });
});


app.listen(port, () => console.log(`Server is now running at http://localhost:${port}`));